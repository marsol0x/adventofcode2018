/* ==================================================================================
   $File: $
   $Date: 01 April 2019 $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */
#include <assert.h>
#include <limits.h>
#include <stdint.h>

#include "common.h"

#define MAX_BUFFER 256

typedef union Vec2
{
    struct
    {
        int64_t x, y;
    };

    struct
    {
        int64_t w, h;
    };
} Vec2;

typedef struct Rect
{
    Vec2 top_left;
    Vec2 bottom_right;
} Rect;

typedef struct Star
{
    Vec2 p;
    Vec2 v;
} Star;

MERGE_SORT_DEFINE(Star);
COMPARE_FUNC(star_compare)
{
    Star a_star = *(Star *)a;
    Star b_star = *(Star *)b;

    return a_star.p.y > b_star.p.y;
}

void print_stars(Star *stars, Rect bounding_box, FILE *output)
{
    int64_t w = bounding_box.bottom_right.x - bounding_box.top_left.x + 1;
    int64_t h = bounding_box.top_left.y - bounding_box.bottom_right.y;

    int64_t buffer_len = w * h * sizeof(char);
    char *output_buffer = xmalloc(buffer_len + 1);
    memset(output_buffer, '.', buffer_len);
    output_buffer[buffer_len] = 0;

    for (int row = 0;
         row < h;
         ++row)
    {
        *(output_buffer + row*w + w) = '\n';
    }

    for (int star_index = 0;
         star_index < buf_len(stars);
         ++star_index)
    {
        Star star = stars[star_index];
        int x = star.p.x - bounding_box.top_left.x;
        int y = star.p.y - bounding_box.bottom_right.y;

        *(output_buffer + y*w + x) = '#';
    }

    fprintf(output, "%s\n", output_buffer);
}

int main(int argc, char **argv)
{
    FILE *input_file = fopen("day10.input", "rb");
    if (!input_file)
    {
        fprintf(stderr, "Failed to open day10.input\n");
        return 1;
    }

    Star *stars = 0;

    char buffer[MAX_BUFFER];
    while (fgets(buffer, MAX_BUFFER, input_file))
    {
        Star star = {0};
        int px, py, vx, vy;
        sscanf(buffer, "position=<%d, %d> velocity=<%d, %d>", &px, &py, &vx, &vy);
        star.p.x = (int64_t)px;
        star.p.y = (int64_t)py;
        star.v.x = (int64_t)vx;
        star.v.y = (int64_t)vy;
        buf_push(stars, star);
    }

    printf("There are %d stars.\n", (int)buf_len(stars));

    int current_second = 0;
    int64_t last_area = INT64_MAX;
    Rect bounding_box = {0};

    for (;;)
    {
        bounding_box.top_left = stars[0].p;
        bounding_box.bottom_right = stars[0].p;

        for (int star_index = 0;
             star_index < buf_len(stars);
             ++star_index)
        {
            Star star = stars[star_index];
            star.p.x += star.v.x;
            star.p.y += star.v.y;
            stars[star_index] = star;

            if (star.p.x < bounding_box.top_left.x)
            {
                bounding_box.top_left.x = star.p.x;
            } 

            if (star.p.x > bounding_box.bottom_right.x)
            {
                bounding_box.bottom_right.x = star.p.x;
            }

            if (star.p.y > bounding_box.top_left.y)
            {
                bounding_box.top_left.y = star.p.y;
            }

            if (star.p.y < bounding_box.bottom_right.y)
            {
                bounding_box.bottom_right.y = star.p.y;
            }
        }

        int64_t w = bounding_box.bottom_right.x - bounding_box.top_left.x + 1;
        int64_t h = bounding_box.top_left.y - bounding_box.bottom_right.y;
        int64_t area = w * h;

        if (last_area >= area)
        {
            last_area = area;
        } else {
            for (int star_index = 0;
                 star_index < buf_len(stars);
                 ++star_index)
            {
                Star star = stars[star_index];
                star.p.x -= star.v.x;
                star.p.y -= star.v.y;
                stars[star_index] = star;
            }
            printf("Second: %d - Area: %ld\n", current_second, last_area);
            print_stars(stars, bounding_box, stdout);
            break;
        }

        ++current_second;
    }
    printf("Final Second: %d\n", current_second);

    return 0;
}
