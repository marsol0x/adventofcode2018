/* ==================================================================================
   $File: day11.c $
   $Date: 21 April 2019 $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */
#include "common.h"

#define SERIAL_NUMBER 3214
#define GRID_SIZE 300

typedef struct Coord
{
    int x, y;
    int power_level;
    int size;
} Coord;

Coord get_max_power_level(int grid[GRID_SIZE][GRID_SIZE], int square_size)
{
    Coord result = {0};
    result.size = square_size;

    for (int y = 0;
         y < GRID_SIZE - square_size;
         ++y)
    {
        for (int x = 0;
             x < GRID_SIZE - square_size;
             ++x)
        {
            int a, b, c, d;

            a = grid[x][y];
            b = grid[x+square_size][y];
            c = grid[x][y+square_size];
            d = grid[x+square_size][y+square_size];

            int power_level = d + a - b - c;

            if (power_level > result.power_level)
            {
                result.x = x;
                result.y = y;
                result.power_level = power_level;
            }
        }
    }

    return result;
}

int main(int argc, char **argv)
{
    int power_grid[GRID_SIZE][GRID_SIZE];
    for (int y = 0;
         y < GRID_SIZE;
         ++y)
    {
        for (int x = 0;
             x < GRID_SIZE;
             ++x)
        {
            int rack_id = x + 1 + 10; // NOTE(marshel): X starts at 1
            int power_level = rack_id * (y + 1); // NOTE(marshel): Y starts at 1
            power_level += SERIAL_NUMBER;
            power_level *= rack_id;

            power_level = (power_level / 100) % 10;
            power_level -= 5;

            power_grid[x][y] = power_level;
        }
    }

    int summed_grid[GRID_SIZE][GRID_SIZE] = {0};
    for (int y = 0;
         y < GRID_SIZE;
         ++y)
    {
        for (int x = 0;
             x < GRID_SIZE;
             ++x)
        {
            int a = power_grid[x][y];
            int b = 0;
            int c = 0;
            int d = 0;

            if (y > 0)
            {
                b = summed_grid[x][y - 1];
                if (x > 0)
                {
                    d = summed_grid[x - 1][y - 1];
                }
            }

            if (x > 0)
            {
                c = summed_grid[x - 1][y];
            }

            summed_grid[x][y] = a + b + c - d;
        }
    }

    putchar('\n');
    Coord top_left = {0};
    top_left.power_level = -1;
    for (int size = 1;
         size <= GRID_SIZE;
         ++size)
    {
        printf("\rChecking %dx%d", size, size);
        fflush(stdout);
        Coord power = get_max_power_level(summed_grid, size);
        if (power.power_level > top_left.power_level)
        {
            top_left = power;
        }
    }
    putchar('\n');

    printf("Coord: (%d, %d), Power Level: %d, Size: %d\n", top_left.x + 2, top_left.y + 2, top_left.power_level, top_left.size);

    return 0;
}
