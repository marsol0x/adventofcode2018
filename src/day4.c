#include <assert.h>
#include <stdio.h>

#include "common.h"

#define BUFFER_SIZE 64
#define MAX_MINUTES 60

enum
{
    RECORD_TYPE_UNKNOWN,
    RECORD_TYPE_GUARD,
    RECORD_TYPE_SLEEP,
    RECORD_TYPE_WAKE,
};

typedef struct Record
{
    int year;
    int month;
    int day;
    int hour;
    int minute;

    int type;
    String event;
} Record;

MERGE_SORT_DEFINE(Record);

COMPARE_FUNC(compare_record)
{
    Record a_record = *(Record *)a;
    Record b_record = *(Record *)b;

    if (a_record.year > b_record.year) return 1;
    if (a_record.year >= b_record.year && a_record.month > b_record.month) return 1;
    if (a_record.year >= b_record.year && a_record.month >= b_record.month && a_record.day > b_record.day) return 1;
    if (a_record.year >= b_record.year && a_record.month >= b_record.month && a_record.day >= b_record.day && a_record.hour > b_record.hour) return 1;
    if (a_record.year >= b_record.year && a_record.month >= b_record.month && a_record.day >= b_record.day && a_record.hour >= b_record.hour && a_record.minute > b_record.minute) return 1;

    return 0;
}

Record parse_record(char *line)
{
    Record result = {0};

    // NOTE(marshel): [YYYY-MM-DD HH:MM] event
    String str = c_string_to_string(line);
    String *split = string_split(str, ' ');

    String *date_split = string_split(*split, '-');
    result.year  = atoi(date_split[0].str + 1);
    result.month = atoi(date_split[1].str);
    result.day   = atoi(date_split[2].str);

    String *time_split = string_split(*(split+ 1), ':');
    result.hour   = atoi(time_split[0].str + 1);
    result.minute = atoi(time_split[1].str);

    if (0) {
    } else if (strncmp(split[2].str, "Guard", split[2].len) == 0) {
        result.type = RECORD_TYPE_GUARD;
    } else if (strncmp(split[2].str, "falls", split[2].len) == 0) {
        result.type = RECORD_TYPE_SLEEP;
    } else if (strncmp(split[2].str, "wakes", split[2].len) == 0) {
        result.type = RECORD_TYPE_WAKE;
    }

    result.event = string_concat(split, ' ', 2, buf_len(split));

    return result;
}

typedef struct Guard
{
    int id;
    int total_minutes_asleep;
    int minutes_asleep[60];
} Guard;

MERGE_SORT_DEFINE(Guard);
COMPARE_FUNC(compare_guard)
{
    Guard *a_guard = (Guard *)a;
    Guard *b_guard = (Guard *)b;

    return a_guard->id > b_guard->id;
}

int get_guard_id(String event)
{
    // NOTE(marshel): Format: Guard #<id> begins shift
    int result = 0;

    String *split = string_split(event, ' ');
    result = atoi(string_to_c_string(split[1]) + 1);

    return result;
}

void print_guard(Guard *guard)
{
    printf("ID: %4d Total Slept: %3d Minutes:", guard->id, guard->total_minutes_asleep);
    for (int i = 0;
         i < MAX_MINUTES;
         ++i)
    {
        printf(" %2d", guard->minutes_asleep[i]);
    }
    printf("\n");
}


int main(int argc, char **argv)
{
    FILE *input_file = fopen("day4.input", "rb");
    if (!input_file)
    {
        fprintf(stderr, "Unable to open input file.\n");
        return 1;
    }

    Record *records = 0;
    char buffer[BUFFER_SIZE] = {0};
    while (fgets(buffer, BUFFER_SIZE, input_file))
    {
        Record record = parse_record(buffer);
        buf_push(records, record);
    }

    MERGE_SORT(Record, records, compare_record);

    Guard *guards = 0;
    Guard *current_guard = 0;
    int sleep_start_minute = 0;
    for (int i = 0;
         i < buf_len(records);
         ++i)
    {
        Record record = records[i];

        switch (record.type)
        {
            case RECORD_TYPE_GUARD:
            {
                current_guard = 0;
                int id = get_guard_id(record.event);
                for (int i = 0;
                     i < buf_len(guards);
                     ++i)
                {
                    Guard *g = guards + i;
                    if (g->id == id)
                    {
                        current_guard = g;
                        break;
                    }
                }

                if (!current_guard)
                {
                    Guard guard = {0};
                    guard.id = id;
                    buf_push(guards, guard);
                    current_guard = guards + buf_len(guards) - 1;
                }
            } break;

            case RECORD_TYPE_SLEEP:
            {
                assert(current_guard);

                sleep_start_minute = record.minute;
            } break;

            case RECORD_TYPE_WAKE:
            {
                assert(current_guard);

                int sleep_end_minute = record.minute;
                for (int i = sleep_start_minute;
                     i < sleep_end_minute;
                     ++i)
                {
                    current_guard->minutes_asleep[i]++;
                    current_guard->total_minutes_asleep++;
                }
            } break;
            
            default:
            {
                assert(!"Invalid default");
            } break;
        }
    }

    Guard header_guard = {0};
    for (int i = 0; i < MAX_MINUTES; ++i) header_guard.minutes_asleep[i] = i;
    print_guard(&header_guard);

    MERGE_SORT(Guard, guards, compare_guard);

    current_guard = guards;
    for (int i = 1;
         i < buf_len(guards);
         ++i)
    {
        Guard *g = guards + i;
        if (g->total_minutes_asleep > current_guard->total_minutes_asleep)
        {
            current_guard = g;
        }
        print_guard(g);
    }

    int most_minute = 0;
    int most_minutes_asleep = 0;
    for (int i = 0;
         i < MAX_MINUTES;
         ++i)
    {
        if (current_guard->minutes_asleep[i] >= most_minutes_asleep)
        {
            most_minute = i;
            most_minutes_asleep = current_guard->minutes_asleep[i];
        }
    }

    printf("\nNum Guards: %d\n", buf_len(guards));
    printf("Selected:\nID: %4d Most Minute Slept: %d\nValue: %d\n", current_guard->id, most_minute, current_guard->id * most_minute);

    // NOTE(marshel): Since my part one implementation prints out a table, it
    // is easy to do part two without writing new code.

    return 0;
}
