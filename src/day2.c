//
//  day2.c
//  adventofcode2018
//
//  Created by Helsper, Marshel J on 12/21/18.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE 32
#define NUM_LETTERS 27

typedef struct Id
{
    struct Id *next, *prev;
    char id[BUFFER_SIZE];
} Id;

#define DLIST_INIT(n) \
    (n)->next = (n); \
    (n)->prev = (n)

#define DLIST_ADD(t, n) \
    (n)->prev = (t)->prev; \
    (n)->next = (t); \
    (n)->prev->next = (n); \
    (t)->prev = (n)

#define DLIST_REMOVE(n) \
    (n)->next->prev = (n)->prev; \
    (n)->prev->next = (n)->next

void * xmalloc(size_t size)
{
    void *result = malloc(size);
    return result;
} 
int get_checksum(FILE *input_file)
{
    int checksum   = 0;
    int num_twice  = 0;
    int num_thrice = 0;

    char buffer[BUFFER_SIZE]  = {0};
    char letters[NUM_LETTERS] = {0};
    while (fgets(buffer, BUFFER_SIZE, input_file))
    {
        for (int i = 0;
             i < BUFFER_SIZE;
             ++i)
        {
            char c = buffer[i];
            if (!c || c == '\n')
            {
                break;
            }

            char l = c - 'a';
            ++letters[l];
        }

        int has_twice = 0;
        int has_thrice = 0;
        for (int i = 0;
             i < NUM_LETTERS;
             ++i)
        {
            if (letters[i] == 2)
            {
                has_twice = 1;
            } else if (letters[i] == 3) {
                has_thrice = 1;
            }
        }
        memset(letters, 0, NUM_LETTERS);

        if (has_twice)
        {
            ++num_twice;
        }

        if (has_thrice)
        {
            ++num_thrice;
        }
    }

    checksum = num_twice * num_thrice;

    return checksum;
}

int main(int argc, char **argv)
{
    FILE *input_file = fopen("day2.input", "rb");
    if (!input_file)
    {
        fprintf(stderr, "Failed to open day2.input\n");
        return 1;
    }

    int checksum = get_checksum(input_file);
    printf("Checksum: %d\n", checksum);

    fseek(input_file, 0, SEEK_SET);

    Id sentinal = {0};
    DLIST_INIT(&sentinal);
    char buffer[BUFFER_SIZE]  = {0};
    while (fgets(buffer, BUFFER_SIZE, input_file))
    {
        Id *node = xmalloc(sizeof(*node));
        memcpy(node->id, buffer, strlen(buffer));

        for (Id *n = sentinal.next;
             n != &sentinal;
             n = n->next)
        {
            int num_diff = 0;
            for (int i = 0;
                 i < strlen(node->id);
                 ++i)
            {
                if (node->id[i] != n->id[i])
                {
                    ++num_diff;
                }

                if (num_diff > 1)
                {
                    break;
                }
            }

            if (num_diff == 1)
            {
                for (int i = 0;
                     i < strlen(node->id);
                     ++i)
                {
                    if (node->id[i] == n->id[i])
                    {
                        printf("%c", node->id[i]);
                    }
                }
                return 0;
            }
        }
        DLIST_ADD(&sentinal, node);
    }

    fclose(input_file);

    return 0;
}
