/* ==================================================================================
   $File: $
   $Date: 20 May 2019 $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

#include "common.h"

#define NUM_RECIPES 323081
i32 sequence[] = {3, 2, 3, 0, 8, 1};

b32 has_sequence(i32 *recipes)
{
    i32 index = buf_len(recipes) - ARRAY_LEN(sequence);
    if (index < 0)
    {
        return 0;
    }

    return recipes[index    ] == sequence[0] &&
           recipes[index + 1] == sequence[1] &&
           recipes[index + 2] == sequence[2] &&
           recipes[index + 3] == sequence[3] &&
           recipes[index + 4] == sequence[4] &&
           recipes[index + 5] == sequence[5];
}

i32 main(i32 arg, char **argv)
{
    i32 *recipes = 0; // NOTE(marshel): Stretchy buf
    i32 first_elf_index = 0;
    i32 second_elf_index = 1;

    // NOTE(marshel): First recipes
    buf_push(recipes, 3);
    buf_push(recipes, 7);

    for (;;)
    {
        i32 new_recipe_score = recipes[first_elf_index] + recipes[second_elf_index];
        i32 first_elf_recipe = new_recipe_score / 10;
        i32 second_elf_recipe = new_recipe_score % 10;

        if (first_elf_recipe != 0)
        {
            buf_push(recipes, first_elf_recipe);
            if (has_sequence(recipes)) break;
        }
        buf_push(recipes, second_elf_recipe);
        if (has_sequence(recipes)) break;

        first_elf_index += recipes[first_elf_index] + 1;
        first_elf_index %= buf_len(recipes);
        
        second_elf_index += recipes[second_elf_index] + 1;
        second_elf_index %= buf_len(recipes);

    }

    printf("Score: %ld\n", buf_len(recipes) - ARRAY_LEN(sequence));

    return 0;
}
