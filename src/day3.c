/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"

#define FABRIC_SIZE 1000
#define BUFFER_SIZE 30
#define NUM_CLAIMS 1287

int fabric[FABRIC_SIZE][FABRIC_SIZE];

typedef struct Vector
{
    union
    {
        struct { int x, y; };
        struct { int w, h; };
    };
} Vector;

typedef struct Claim
{
    int id;
    int overlap;
    Vector pos;
    Vector dim;
} Claim;
Claim *claims; // NOTE(marshel): Stretchy buffer

int second_space_position(char *buffer)
{
    int num_space = 0;
    int i = 0;
    for (i = 0;
         i < strlen(buffer);
         ++i)
    {
        if (buffer[i] == ' ')
        {
            ++num_space;
            if (num_space == 2)
            {
                break;
            }
        }
    }

    return i;
}

Vector get_offset(char *offset_string)
{
    Vector result;

    char *left_edge = offset_string;
    char *comma = strstr(offset_string, ",");
    *comma = 0;
    char *top_edge = comma + 1;

    result.x = atoi(left_edge);
    result.y = atoi(top_edge);

    return result;
}

Vector get_dimension(char *dimension_string)
{
    Vector result;

    char *x = strstr(dimension_string, "x");
    *x = 0;
    char *width = dimension_string;
    char *height = x + 1;

    result.w = atoi(width);
    result.h = atoi(height);

    return result;
}

Claim claim_from_c_string(char *buffer, int size)
{
    // NOTE(marshel): #<id> @ <posX>,<posY>: <dimX>x<dimY>\n
    Claim result = {0};

    if (buffer && *buffer == '#')
    {
        String string = c_string_to_string(buffer);
        String *split_string = string_split(string, ' ');

        int id = atoi(string_to_c_string(split_string[0]) + 1);
        Vector pos = get_offset(string_to_c_string(split_string[2]));
        Vector dim = get_dimension(string_to_c_string(split_string[3]));

        result.id = id;
        result.pos = pos;
        result.dim = dim;
    }

    return result;
}

int main(int argc, char **argv)
{
    FILE *input_file = fopen("day3.input", "rb");
    if (!input_file)
    {
        printf("Failed to open day3.input.\n");
        return 1;
    }

    char buffer[BUFFER_SIZE] = {0};
    while (fgets(buffer, BUFFER_SIZE, input_file))
    {
        Claim claim = claim_from_c_string(buffer, BUFFER_SIZE);
        buf_push(claims, claim);

        for (int y = claim.pos.y;
             y < claim.pos.y + claim.dim.h;
             ++y)
        {
            for (int x = claim.pos.x;
                 x < claim.pos.x + claim.dim.w;
                 ++x)
            {
                if (fabric[x][y])
                {
                    Claim *existing_claim = &claims[fabric[x][y] - 1];
                    Claim *current_claim = &claims[claim.id - 1];
                    existing_claim->overlap = 1;
                    current_claim->overlap = 1;
                }
                fabric[x][y] = claim.id;
            }
        }
    }

    for (int i = 0;
         i < buf_len(claims);
         ++i)
    {
        if (claims[i].overlap == 0)
        {
            printf("No Overlap ID: %d\n", claims[i].id);
            break;
        }
    }

#if 0
    int num_overlap = 0;
    for (int y = 0;
         y < FABRIC_SIZE;
         ++y)
    {
        for (int x = 0;
             x < FABRIC_SIZE;
             ++x)
        {
            if (fabric[x][y] > 1)
            {
                ++num_overlap;
            }
        }
    }

    printf("Num Overlap: %d\n", num_overlap);
#endif

    fclose(input_file);

    return 0;
}
