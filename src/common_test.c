/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

#include <assert.h>
#include "common.h"

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

MERGE_SORT_DEFINE(int);

COMPARE_FUNC(compare_int)
{
    int a_int = *(int *)a;
    int b_int = *(int *)b;

    return a_int > b_int;
}

typedef struct Foo
{
    int i;
} Foo;
#define MAX_FOO 10
Foo foo_list[MAX_FOO];

void stretchy_buf_test(void)
{
    int *buf = 0;
    int n = 1024;
    for (int i = 0;
         i < n;
         ++i)
    {
        buf_push(buf, i);
    }

    for (int i = 0;
         i < n;
         ++i)
    {
        assert(buf[i] == i);
    }

    buf_free(buf);
    assert(buf == 0);
    assert(buf_len(buf) == 0);
}

void merge_sort_test(void)
{
    srand((unsigned int)time(0));
    int *random_buf = 0;
    int n = 1024;
    for (int i = 0;
         i < n;
         ++i)
    {
        int val = rand();
        buf_push(random_buf, val);
    }

    MERGE_SORT(int, random_buf, compare_int);

    for (int i = 1;
         i < n;
         ++i)
    {
        assert(random_buf[i - 1] <= random_buf[i]);
    }
    buf_free(random_buf);
}

void double_stretchy_buf_test(void)
{
    for (int i = 0;
         i < MAX_FOO;
         ++i)
    {
        foo_list[i].i = MAX_FOO - i;
    }

    Foo **foo_buf = 0;
    for (int i = 0;
         i < MAX_FOO;
         ++i)
    {
        buf_push(foo_buf, foo_list + i);
    }

    for (int i = 0;
         i < buf_len(foo_buf);
         ++i)
    {
        assert(foo_buf[i]->i == MAX_FOO - i);
    }
}

void map_test(void)
{
    u64 n = 1024;
    Map map = {0};

    for (u64 i = 1;
         i < n;
         ++i)
    {
        map_put_u64_to_u64(&map, i, i+1);
    }

    for (u64 i = 1;
         i < n;
         ++i)
    {
        u64 val = map_get_u64(&map, i);
        assert(val == i + 1);
    }
}

void string_intern_test(void)
{
    Map intern_map = {0};
    char *name = "Marshel";
    char *interned_name = string_intern_range(&intern_map, name, name + strlen(name));
    char *returned_name = string_intern_range(&intern_map, name, name + strlen(name));
    assert(interned_name == returned_name);
}

int main(int argc, char **argv)
{
    stretchy_buf_test();
    merge_sort_test();
    double_stretchy_buf_test();
    map_test();
    string_intern_test();

    printf("Success!\n");

    return 0;
}
