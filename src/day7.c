//
//  day7.c
//  Advent of Code
//
//  Created by Helsper, Marshel J on 2/1/19.
//

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"

#define MAX_BUFFER_SIZE 4096
#define MAX_STEPS 26
#define MAX_WORKERS 5


typedef struct Step
{
    struct Step **prev; // NOTE(marshel): Stretchy buffer
    struct Step **next; // NOTE(marshel): Stretchy buffer
    int execution_time;
    char name;
} Step;
Step all_steps[MAX_STEPS];

int total_elapsed_time;

#define STEP_INDEX(s) (int)((s) - 'A')
#define STEP_GET(s) all_steps[STEP_INDEX(s)]

int step_compare(const void *a_ptr, const void *b_ptr)
{
    Step a = *(Step *)a_ptr;
    Step b = *(Step *)b_ptr;

    return a.name > b.name;
}

int indirect_step_compare(const void *a_ptr, const void *b_ptr)
{
    Step *a = *(Step **)a_ptr;
    Step *b = *(Step **)b_ptr;

    return a->name > b->name;
}

void step_add(Step *sentinal, char current_step_name, char predicate_step_name)
{
    Step *current_step   = &STEP_GET(current_step_name);
    Step *predicate_step = &STEP_GET(predicate_step_name);

    current_step->execution_time = STEP_INDEX(current_step_name) + 61;
    predicate_step->execution_time = STEP_INDEX(predicate_step_name) + 61;

    buf_push(predicate_step->next, current_step);
    buf_push(current_step->prev, predicate_step);

    qsort(predicate_step->next, buf_len(predicate_step->next), sizeof(Step*), step_compare);
    qsort(current_step->prev,   buf_len(current_step->prev),   sizeof(Step*), step_compare);
}

Step *step_workers[MAX_WORKERS];
int active_workers;
Step **completed_work;
Step **no_incoming_edges;
Step **sorted_steps;
void kahns_sort(void)
{
    for (int step_index = 0;
         step_index < MAX_STEPS;
         ++step_index)
    {
        Step *step = &all_steps[step_index];
        if (!buf_len(step->prev))
        {
            buf_push(no_incoming_edges, step);
        }
    }

    int has_work = 1;
    while (buf_len(no_incoming_edges) || has_work)
    {
        Step *step = 0;
        while (active_workers < MAX_WORKERS && buf_len(no_incoming_edges))
        {
            qsort(no_incoming_edges, buf_len(no_incoming_edges), sizeof(Step*), indirect_step_compare);

            step = no_incoming_edges[0];
            for (int worker_index = 0;
                 worker_index < MAX_WORKERS;
                 ++worker_index)
            {
                if (step_workers[worker_index] == 0)
                {
                    step_workers[worker_index] = step;
                    buf_remove(no_incoming_edges, 0);
                    ++active_workers;
                    break;
                }
            }
        }

        has_work = 0;
        int did_time = 0;
        for (int worker_index = 0;
             worker_index < MAX_WORKERS;
             ++worker_index)
        {
            Step *step = step_workers[worker_index];
            if (step)
            {
                step->execution_time--;
                did_time = 1;
                if (step->execution_time == 0)
                {
                    buf_push(sorted_steps, step);
                    buf_push(completed_work, step);
                    step_workers[worker_index] = 0;
                    --active_workers;
                } else {
                    has_work = 1;
                }
            }
        }

        if (did_time)
        {
            ++total_elapsed_time;
        }

        while (buf_len(completed_work))
        {
            if (buf_len(completed_work) > 1)
            {
                qsort(completed_work, buf_len(completed_work), sizeof(Step*), indirect_step_compare);
            }

            step = completed_work[0];
            buf_remove(completed_work, 0);
            while (buf_len(step->next))
            {
                Step *next_step = step->next[0];

                for (int prev_step_index = 0;
                    prev_step_index < buf_len(next_step->prev);
                    ++prev_step_index)
                {
                    Step *prev_step = next_step->prev[prev_step_index];
                    if (prev_step->name == step->name)
                    {
                        buf_remove(next_step->prev, prev_step_index);
                        break;
                    }
                }

                if (buf_len(next_step->prev) == 0)
                {
                    buf_push(no_incoming_edges, next_step);
                }

                buf_remove(step->next, 0);
            }
        }
    }
}

void print_sorted(void)
{
    for (int sorted_index = 0;
         sorted_index < buf_len(sorted_steps);
         ++sorted_index)
    {
        Step *step = sorted_steps[sorted_index];
        printf("%c", step->name);
    }
    putchar('\n');
}

int main(int argc, char **argv)
{
    for (int step_index = 0;
         step_index < MAX_STEPS;
         ++step_index)
    {
        all_steps[step_index].name = 'A' + step_index;
    }

    FILE *input_file = fopen("day7.input", "rb");
    if (!input_file)
    {
        fprintf(stderr, "Failed to open day7.input.\n");
        return 1;
    }

    Step step_sentinal = {0};

    char buffer[MAX_BUFFER_SIZE];
    while (fgets(buffer, MAX_BUFFER_SIZE, input_file))
    {
        String line = c_string_to_string(buffer);
        String *parts = string_split(line, ' ');

        step_add(&step_sentinal, parts[7].str[0], parts[1].str[0]);
    }

    kahns_sort();
    printf("Kahns: ");
    print_sorted();

    printf("Total Elapsed Time: %d\n", total_elapsed_time);

    return 0;
}
