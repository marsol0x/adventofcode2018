/* ==================================================================================
   $File: $
   $Date: 22 April 2019 $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */
#include "common.h"

#define NUM_GENERATIONS 10021 //50000000000

char * eat_whitespace(char *c)
{
    while (isspace(*c))
        ++c;

    return c;
}

char * get_initial_state(char *data, char **end)
{
    char *result = 0;
    char *pos = data;
    while (*pos++ != ':')
        ;
    ++pos;

    while (!isspace(*pos))
    {
        buf_push(result, *pos);
        ++pos;
    }

    if (end)
    {
        *end = pos;
    }

    return result;
}

Map rules_strings;

void set_rules(Map *rules, char *data)
{
    // NOTE(marshel): Rule format:
    //  Rule     Result
    //  -----    -
    //  LLCRR => N
    while (*data)
    {
        data = eat_whitespace(data);
        char *rule = string_intern_range(&rules_strings, data, data + 5);
        data += 9;
        char result = *data;
        data += 2;

        map_put_u64_to_u64(rules, (u64)(uintptr_t)rule, (u64)result);
    }
}

char get_state(char *state, i32 index, i32 offset)
{
    char result = '.';

    index -= offset;

    if (index >= 0 && index < buf_len(state))
    {
        result = state[index];
    }

    return result;
}

char * tick_generation(Map *rules, char *state, char *result, i32 *first_pot_index)
{
    char rule_to_check[5];

    i32 has_first_plot = 0;
    i32 first_pot = *first_pot_index;
    i32 start = first_pot - 4; // NOTE(marshel): Start 4-back from the first live pot
    i32 end = first_pot + buf_len(state) + 4; // NOTE(marshel): Only need to go 2-forward from the last pot
    for (i32 index = start;
         index < end;
         ++index)
    {
        rule_to_check[0] = get_state(state, index - 2, first_pot); // L
        rule_to_check[1] = get_state(state, index - 1, first_pot); // L
        rule_to_check[2] = get_state(state, index, first_pot);     // C
        rule_to_check[3] = get_state(state, index + 1, first_pot); // R
        rule_to_check[4] = get_state(state, index + 2, first_pot); // R

        char *rule_key   = string_intern_range(&rules_strings, rule_to_check, rule_to_check + 5);
        char rule_result = (char)map_get_u64(rules, (u64)(uintptr_t)rule_key);
        rule_result      = rule_result ? rule_result : '.';

        if (rule_result == '#')
        {
            buf_push(result, rule_result);
            if (!has_first_plot) //index <= 0 && index < *first_pot_index)
            {
                *first_pot_index = index;
                has_first_plot = 1;
            }
        } else {
            if ((index >= first_pot) || (index < buf_len(state)))
            {
                buf_push(result, rule_result);
            }
        }
    }

    // NOTE(marshel): Removing empty pots at the end helps with memory usage
    for (i32 index = buf_len(result) - 1;
         index >= 0;
         --index)
    {
        if (result[index] == '#')
        {
            break;
        }

        buf__header(result)->len--;
    }

    while (result[0] == '.')
    {
        buf_remove(result, 0);
    }

    return result;
}

i32 score_state(char *state, i32 first_pot_index)
{
    i32 score = 0;
    i32 value = first_pot_index;
    for (i32 index = 0;
         index < buf_len(state);
         ++index)
    {
        if (state[index] == '#')
        {
            score += value;
        }
        ++value;
    }

    return score;
}

int main(i32 argc, char **argv)
{
    FILE *input_file = fopen("day12.input", "rb");
    if (!input_file)
    {
        fprintf(stderr, "Failed to open day12.input\n");
        return 1;
    }
    char *input_data = read_entire_file(input_file);
    fclose(input_file);

    // NOTE(marshel):
    // First line is the initial state, then a blank line, then each rule
    // separated by a newline
    Map rules = {0};
    char *rules_start = 0;
    char *state = get_initial_state(input_data, &rules_start); // NOTE(marshel): Stretchy-buf
    char *new_state = 0;
    set_rules(&rules, rules_start);

    i32 first_pot_index = 0;
    for (i64 gen = 0;
         gen < NUM_GENERATIONS;
         ++gen)
    {
        printf("%5lld [%4d] [%5d] %.*s\n", gen, first_pot_index, score_state(state, first_pot_index), (i32)buf_len(state), state);
        new_state = tick_generation(&rules, state, new_state, &first_pot_index);

        // NOTE(marshel): Reduce allocations by recycling state arrays
        char *t = state;
        state = new_state;
        new_state = t;

        buf_clear(new_state);
    }

    printf("Score: %d\n", score_state(state, first_pot_index));

    // NOTE(marshel): Note about Part 2
    //  m = 42;
    //  b = 428;
    //  part2_score = m * 50,000,000,000 + b;
    //
    //  This comes from how the state stablizes over time (which can easily be
    //  seen at 10k iterations). We find that for each generation the score
    //  increased by 42, from a base of 428. We can therefore just calculate
    //  the score after 50 billion iterations without actually computing the
    //  state.

    return 0;
}
