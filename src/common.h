#ifndef COMMON_H
/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

#include <assert.h>
#include <ctype.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef int8_t   i8;
typedef int16_t  i16;
typedef int32_t  i32;
typedef int64_t  i64;
typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef float    r32;
typedef double   r64;

typedef i32 b32;

#define ARRAY_LEN(a) (sizeof(a) / sizeof((a)[0]))

//
// NOTE(marshel): Memory allocation stuff for debugging purposes
//

void * xmalloc(size_t size)
{
    void *result = malloc(size);

    return result;
}

void * xcalloc(size_t num, size_t size)
{
    void *result = calloc(num, size);

    return result;
}

void * xrealloc(void *ptr, size_t new_size)
{
    void *result = realloc(ptr, new_size);

    return result;
}

//
// NOTE(marshel): File stuff
//
char * read_entire_file(FILE* file)
{
    char *result = 0;
    int file_length = 0;

    fseek(file, 0, SEEK_END);
    file_length = ftell(file);
    fseek(file, 0, SEEK_SET);

    result = xmalloc(file_length);
    fread(result, 1, file_length + 1, file);
    result[file_length] = 0;

    for (int index = file_length - 1;
         index > 0;
         --index)
    {
        if (isspace(result[index]))
        {
            result[index] = 0;
        } else {
            break;
        }
    }

    return result;
}

//
// NOTE(marshel): Linked List
//
#define DLIST_INIT(n) \
    (n)->prev = (n); \
    (n)->next = (n)

#define DLIST_ADD(l, n) \
    (n)->next = (l); \
    (n)->prev = (l)->prev; \
    (l)->prev->next = (n); \
    (l)->prev = (n)

#define DLIST_REMOVE(n) \
    (n)->next->prev = (n)->prev; \
    (n)->prev->next = (n)->next

//
// NOTE(marshel): Stretchy Buffers
//

typedef struct BufferHeader
{
    size_t len;
    size_t cap;
    size_t elem_size;
    char buf[];
} BufferHeader;

#define MIN(a, b) ((a) > (b) ? (b) : (a))
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define CLAMP_MAX(a, max) MIN(a, max)
#define CLAMP_MIN(a, min) MAX(a, min)

#define buf__header(b) ((BufferHeader *)((char *)(b) - offsetof(BufferHeader, buf)))

#define buf_len(b) ((b) ? buf__header(b)->len : 0)
#define buf_cap(b) ((b) ? buf__header(b)->cap : 0)
#define buf_fit(b, n) ((n) <= buf_cap(b) ? 0 : ((b) = buf__grow((b), (n), sizeof(*(b)))))
#define buf_push(b, ...) (buf_fit((b), 1 + buf_len(b)), (b)[buf__header(b)->len++] = (__VA_ARGS__))
#define buf_free(b) ((b) ? (free(buf__header(b)), (b) = 0) : 0)
#define buf_clear(b) ((b) ? buf__header(b)->len = 0 : 0)

void * buf__grow(void *buf, size_t new_len, size_t elem_size)
{
    size_t new_cap = CLAMP_MIN(buf_cap(buf) + (buf_cap(buf) / 2), MAX(new_len, 16));
    size_t new_size = offsetof(BufferHeader, buf) + (new_cap * elem_size);

    BufferHeader *new_header;
    if (buf)
    {
        new_header = xrealloc(buf__header(buf), new_size);
        assert(new_header);
    } else {
        new_header = xmalloc(new_size);
        new_header->len = 0;
        new_header->elem_size = elem_size;
    }
    new_header->cap = new_cap;

    return new_header->buf;
}

void buf_remove(void *buf, int index)
{
    if (buf && buf_len(buf) > index)
    {
        char *src = (char *)buf + ((index + 1) * buf__header(buf)->elem_size);
        char *dest = (char *)buf + (index * buf__header(buf)->elem_size);
        size_t count = buf__header(buf)->elem_size * (buf_len(buf) - index);
        memcpy(dest, src, count);
        buf__header(buf)->len--;
    }
}

void * buf_copy(void *src)
{
    BufferHeader *result = 0;
    BufferHeader *src_header = buf__header(src);
    size_t size = sizeof(BufferHeader) + (src_header->cap * src_header->elem_size);
    result = xmalloc(size);

    result->len = src_header->len;
    result->cap = src_header->cap;
    result->elem_size = src_header->elem_size;

    memcpy(result->buf, src, src_header->len * src_header->elem_size);

    return result->buf;
}

//
// NOTE(marshel): Hash Map
//

u64 hash_u64(u64 val)
{
    u64 h = val;

    h ^= h >> 33;
    h *= 0xff51afd7ed558ccdull;
    h ^= h >> 33;
    h *= 0xc4ceb9fe1a85ec53ull;
    h ^= h >> 33;

    return h;
}

u64 hash_bytes(const void *ptr, size_t len)
{
    // NOTE(marshel) FNV1a
    u64 result = 0;
    u64 fnv_bias = 0xcbf29ce484222325ull;
    u64 fnv_prime = 0x100000001b3ull;
    const char *buf = (const char *)ptr;

    result = fnv_bias;
    for (size_t index = 0;
         index < len;
         ++index)
    {
        result ^= buf[index];
        result *= fnv_prime;
    }
    
    return result;
}

typedef struct Map
{
    u64 *keys;
    u64 *vals;
    size_t cap;
    size_t len;
} Map;

void map_put_u64_to_u64(Map *map, u64 key, u64 val);

void map_grow(Map *map, size_t new_cap)
{
    new_cap = CLAMP_MIN(new_cap, 16);
    Map new_map = {
        .keys = xcalloc(new_cap, sizeof(u64)),
        .vals = xmalloc(new_cap * sizeof(u64)),
        .cap = new_cap,
        .len = 0,
    };

    for (size_t i = 0;
         i < map->cap;
         ++i)
    {
        if (map->keys[i])
        {
            map_put_u64_to_u64(&new_map, map->keys[i], map->vals[i]);
        }
    }

    free(map->keys);
    free(map->vals);
    *map = new_map;
}

void map_put_u64_to_u64(Map *map, u64 key, u64 val)
{
    assert(key);
    if (!val)
    {
        return;
    }

    if (2*map->len >= map->cap)
    {
        map_grow(map, 2*map->cap);
    }

    u64 hash = hash_u64(key);
    u64 index = hash % map->cap;
    for (;;)
    {
        if (!map->keys[index])
        {
            map->keys[index] = key;
            map->vals[index] = val;
            ++map->len;
            return;
        } else if (map->keys[index] == key) {
            map->vals[index] = val;
            return;
        }

        ++index;
        if (index == map->cap)
        {
            index = 0;
        }
    }
}

void map_put_u64(Map *map, u64 key, void *val)
{
    map_put_u64_to_u64(map, key, (u64)(uintptr_t)val);
}

void map_put(Map *map, void *key, void *val)
{
    map_put_u64_to_u64(map, (u64)(uintptr_t)key, (u64)(uintptr_t)val);
}

u64 map_get_u64(Map *map, u64 key)
{
    if (map->len == 0)
    {
        return 0;
    }

    u64 hash = hash_u64(key);
    u64 index = hash % map->cap;
    for (;;)
    {
        if (map->keys[index] == key)
        {
            return map->vals[index];
        } else if (!map->keys[index]) {
            return 0;
        }

        ++index;
        if (index == map->cap)
        {
            index = 0;
        }
    }
}

void * map_get(Map *map, u64 key)
{
    void *val = (void *)(uintptr_t)map_get_u64(map, key);
    return val;
}

//
// NOTE(marshel): Strings
//

// TODO(marshel): Possibly replace with a header-style string similar to
// stretchy bufs? Ginger Bill did this.
typedef struct String
{
    size_t len;
    char *str;
} String;

String c_string_to_string(char *c_string) // TODO(marshel): LEAK, user is responsible for freeing strings
{
    String result;
    size_t len = strlen(c_string);

    result.str = xmalloc(len + 1);
    memcpy(result.str, c_string, len);
    result.len = len;
    result.str[len] = 0;

    return result;
}

#define string_to_c_string(s) (s).str
#define string_format(s) (s)->len, (s)->str

String * string_split(String string, char sep)
{
    String *result = 0;

    int i = 0;
    while (i < string.len)
    {
        String new_string = {0};

        char *start = string.str + i;
        char *end = start;
        while (i < string.len && string.str[i] != sep) ++i;
        end = string.str + i;

        new_string.len = end - start;
        if (new_string.len)
        {
            new_string.str = xmalloc(new_string.len);
            memcpy(new_string.str, start, new_string.len);
            buf_push(result, new_string);
            ++i;
        }
    }

    return result;
}

#define string_free(s) free((s).str); (s).str = 0; (s).len = 0

String string_concat(String *strings, char sep, int start, int end)
{
    size_t length = 0;
    for (int i = start;
         i < end;
         ++i)
    {
        length += strings[i].len;
    }

    length += end - start;

    String result = {0};
    result.len = length;
    result.str = xmalloc(length + 1);

    size_t count = 0;
    for (int i = start;
         i < end;
         ++i)
    {
        count += snprintf(result.str + count, length - count, "%.*s%c", (int)strings[i].len, strings[i].str, sep);
    }

    return result;
}

//
// NOTE(marshel): String Interning
//
typedef struct Intern
{
    size_t len;
    struct Intern *next;
    char str[0];
} Intern;

char * string_intern_range(Map *intern_map, char *start, char *end)
{
    size_t len = end - start;
    u64 hash = hash_bytes(start, len);
    u64 key = hash ? hash : 1;

    Intern *intern = (Intern *)map_get(intern_map, key);
    for (Intern *it = intern;
         it;
         it = it->next)
    {
        if (it->len == len && strncmp(it->str, start, len) == 0)
        {
            return it->str;
        }
    }

    Intern *new_intern = xmalloc(sizeof(*new_intern) + len + 1);
    new_intern->len = len;
    new_intern->next = intern;
    memcpy(new_intern->str, start, len);
    new_intern->str[len] = 0;
    map_put_u64(intern_map, key, new_intern);
    return new_intern->str;
}

//
// NOTE(marshel): Sorting
//
#define COMPARE_FUNC(name) int name(void *a, void *b)
typedef COMPARE_FUNC(CompareFunction);

#define MERGE_SORT_DEFINE(TYPE)                                                                                     \
void merge_sort_merge_##TYPE(TYPE *src, TYPE *dest, int left, int middle, int right, CompareFunction *compare_func) \
{                                                                                                                   \
    int i = left;                                                                                                   \
    int j = middle;                                                                                                 \
                                                                                                                    \
    for (int k = left;                                                                                              \
         k < right;                                                                                                 \
         ++k)                                                                                                       \
    {                                                                                                               \
        if (i < middle && (j >= right || compare_func(src + i, src + j) <= 0))                                      \
        {                                                                                                           \
            dest[k] = src[i];                                                                                       \
            ++i;                                                                                                    \
        } else {                                                                                                    \
            dest[k] = src[j];                                                                                       \
            ++j;                                                                                                    \
        }                                                                                                           \
    }                                                                                                               \
}                                                                                                                   \
                                                                                                                    \
void merge_sort_split_##TYPE(TYPE *src, TYPE *dest, int left, int right, CompareFunction *compare_func)             \
{                                                                                                                   \
    if (right - left < 2)                                                                                           \
        return;                                                                                                     \
                                                                                                                    \
    int middle = (left + right) / 2;                                                                                \
    merge_sort_split_##TYPE(dest, src, left,   middle, compare_func);                                               \
    merge_sort_split_##TYPE(dest, src, middle, right,  compare_func);                                               \
                                                                                                                    \
                                                                                                                    \
    merge_sort_merge_##TYPE(src, dest, left, middle, right, compare_func);                                          \
}                                                                                                                   \
                                                                                                                    \
void merge_sort_##TYPE(TYPE *src, CompareFunction *compare_func)                                                    \
{                                                                                                                   \
    TYPE *dest = 0;                                                                                                 \
    for (int i = 0;                                                                                                 \
         i < buf_len(src);                                                                                          \
         ++i)                                                                                                       \
    {                                                                                                               \
        buf_push(dest, src[i]);                                                                                     \
    }                                                                                                               \
                                                                                                                    \
    merge_sort_split_##TYPE(dest, src, 0, buf_len(src), compare_func);                                              \
    buf_free(dest);                                                                                                 \
}

#define MERGE_SORT(TYPE, arr, compare_func) merge_sort_##TYPE(arr, compare_func)

#define COMMON_H
#endif
