/* ==================================================================================
   $File: $
   $Date: 28 March 2019 $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "common.h"

char *stream;
int total_metadata;

void consume_whitespace(void)
{
    while (*stream && isspace(*stream))
    {
        ++stream;
    }
}

void next_whitespace(void)
{
    while (*stream && !isspace(*stream))
    {
        ++stream;
    }
}

typedef struct LicenseNode
{
    int value;
    int metadata_sum;
    int *metadata;
    struct LicenseNode **children;
} LicenseNode;

#define GET_ENTRY(var) \
    consume_whitespace(); \
    (var) = stream; \
    next_whitespace(); \
    *stream++ = 0

void create_tree(LicenseNode *parent)
{
    if (*stream)
    {
        char *entry;
        int num_children = 0;
        int num_metadata = 0;

        // NOTE(marshel): First number is the number of children
        GET_ENTRY(entry);
        num_children = atoi(entry);

        // NOTE(marshel): Second number is the number of metadata entries
        GET_ENTRY(entry);
        num_metadata = atoi(entry);

        LicenseNode *node = xmalloc(sizeof(*node));
        node->value = 0;
        node->metadata_sum = 0;
        node->metadata = 0;
        node->children = 0;

        buf_push(parent->children, node);

        if (num_children)
        {
            for (int child_index = 0;
                 child_index < num_children;
                 ++child_index)
            {
                create_tree(node);
            }
        }

        for (int metadata_index = 0;
             metadata_index < num_metadata;
             ++metadata_index)
        {
            GET_ENTRY(entry);
            int metadata_value = atoi(entry);
            buf_push(node->metadata, metadata_value);
            node->metadata_sum += metadata_value;
        }
        total_metadata += node->metadata_sum;

        if (num_children)
        {
            for (int metadata_index = 0;
                 metadata_index < buf_len(node->metadata);
                 ++metadata_index)
            {
                int child_index = node->metadata[metadata_index] - 1;
                if (child_index >= 0 && child_index < buf_len(node->children))
                {
                    node->value += node->children[child_index]->value;
                }
            }
        } else {
            node->value = node->metadata_sum;
        }

        consume_whitespace();
    }
}

int main(int argc, char **argv)
{
    FILE *input_file = fopen("day8.input", "rb");
    if (!input_file)
    {
        fprintf(stderr, "Failed to open day8.input.\n");
        return 1;
    }

    char *input_data = read_entire_file(input_file);
    fclose(input_file);

    stream = input_data;
    LicenseNode root = {0};
    create_tree(&root);

    printf("Sum of Metadata: %d\n", total_metadata);
    printf("Value of root node: %d\n", root.children[0]->value);

    return 0;
}
