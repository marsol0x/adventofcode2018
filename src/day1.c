#if 0
clang -g -O0 -std=c99 -o ./bin/day_01 day_01.c
exit
#endif
/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: (C) Copyright YEAR by Dun & Bradstreet NetProspex. All Rights Reserved. $
   ================================================================================== */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_BUFFER 128

void * xmalloc(size_t size)
{
    return malloc(size);
}

typedef struct freq_tree
{
    struct freq_tree *left, *right;
    int32_t freq;
} freq_tree;

freq_tree * tree_new_node(freq_tree *root, int32_t value)
{
    freq_tree *new_node = (freq_tree *)xmalloc(sizeof(*new_node));
    new_node->freq = value;
    new_node->left = new_node->right = 0;

    if (root)
    {
        freq_tree *node = root;
        freq_tree *prev_node;
        while (node)
        {
            prev_node = node;
            if (node->freq > value)
            {
                node = node->left;
            } else {
                node = node->right;
            }
        }

        if (prev_node->freq > value)
        {
            prev_node->left = new_node;
        } else {
            prev_node->right = new_node;
        }
    }

    return new_node;
}

freq_tree * tree_find_node(freq_tree *root, int32_t value)
{
    freq_tree *result = 0;
    if (root)
    {
        result = root;
        while (result)
        {
            if (result->freq == value)
            {
                break;
            }

            if (result->freq > value)
            {
                result = result->left;
            } else {
                result = result->right;
            }
        }
    }

    return result;
}

int32_t current_frequency;
int32_t end_of_stream;

int32_t get_next_frequency(FILE *stream, char *buffer)
{
    if (fgets(buffer, MAX_BUFFER, stream) != NULL)
    {
        int32_t change = atoi(buffer);
        return change;
    } else {
        end_of_stream = 1;
        return 0;
    }
}

int32_t main(int32_t args, char **argv)
{
    char *input_file = "./input/day1.input";
    current_frequency = 0;
    end_of_stream = 0;

    freq_tree *root_node = 0;

    char buffer[MAX_BUFFER] = {0};
    FILE *input = fopen(input_file, "rb");

    for (;;)
    {
        int32_t change = get_next_frequency(input, buffer);
        if (end_of_stream != 0)
        {
            break;
        }
        current_frequency += change;

        freq_tree *new_node = tree_new_node(root_node, current_frequency);
        if (!root_node) root_node = new_node;
    }
    printf("Frequency: %d\n", current_frequency);

    for (;;)
    {
        fseek(input, 0, SEEK_SET);
        end_of_stream = 0;
        for (;;)
        {
            int32_t change = get_next_frequency(input, buffer);
            if (end_of_stream != 0)
            {
                break;
            }

            current_frequency += change;
            freq_tree *repeat_node = tree_find_node(root_node, current_frequency);
            if (repeat_node)
            {
                printf("Repeat Frequency: %d\n", repeat_node->freq);
                goto end;
            }
        }
    }
end:

    fclose(input);

    return 0;
}
