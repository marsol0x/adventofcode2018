/* ==================================================================================
   $File: $
   $Date: 01 April 2019 $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

#include <stdio.h>

#include "common.h"

#define NUM_PLAYERS 476
#define MAX_MARBLE 7143100L

unsigned long long player_scores[NUM_PLAYERS];

typedef struct Marble
{
    struct Marble *prev, *next;
    unsigned long long value;
} Marble;

void marble_print_circle(Marble *circle, Marble *current)
{
    Marble *marble = circle;
    do
    {
        if (marble == current)
        {
            printf("(%lld) ", marble->value);
        } else {
            printf("%lld ", marble->value);
        }
        marble = marble->next;
    } while (marble != circle);
    putchar('\n');
}

Marble * marble_new(int value)
{
    Marble *result = xmalloc(sizeof(*result));
    result->prev = result->next = 0;
    result->value = value;

    return result;
}

int main(int argc, char **argv)
{
    Marble marble_circle = {0};
    DLIST_INIT(&marble_circle);

    int current_player = 0;
    Marble *current_marble = &marble_circle;

    //marble_print_circle(&marble_circle, current_marble);
    for (int marble_value = 1;
         marble_value <= MAX_MARBLE;
         ++marble_value)
    {
        if (marble_value % 23 == 0)
        {
            player_scores[current_player] += marble_value;
            Marble *marble_remove = current_marble;
            for (int marble_count = 0;
                 marble_count < 7;
                 ++marble_count)
            {
                marble_remove = marble_remove->prev;
            }
            player_scores[current_player] += marble_remove->value;
            current_marble = marble_remove->next;
            DLIST_REMOVE(marble_remove);
            free(marble_remove);
        } else {
            Marble *marble = marble_new(marble_value);
            Marble *marble_insert = current_marble->next;

            marble->prev = marble_insert;
            marble->next = marble_insert->next;
            marble->next->prev = marble;
            marble->prev->next = marble;

            current_marble = marble;
        }
        //marble_print_circle(&marble_circle, current_marble);

        ++current_player;
        if (current_player == NUM_PLAYERS)
        {
            current_player = 0;
        }
    }

    int player_winner = 0;
    for (int player = 0;
         player < NUM_PLAYERS;
         ++player)
    {
        if (player_scores[player] > player_scores[player_winner])
        {
            player_winner = player;
        }
    }
    printf("Player %d won with a score of %lld\n", player_winner + 1, player_scores[player_winner]);

    return 0;
}
