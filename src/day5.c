/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

#include <stdio.h>
#include <ctype.h>
#include <string.h>

#include "common.h"

#define BUFFER_SIZE 4096

void react(char *units)
{
    int did_react;
    do
    {
        did_react = 0;
        for (int i = 1;
             i < buf_len(units);
             ++i)
        {
            char unit_a = units[i - 1];
            char unit_b = units[i];

            if (unit_a == unit_b) continue;
            if (tolower(unit_a) == tolower(unit_b))
            {
                // NOTE(marshel): Reaction!
                buf_remove(units, i--);
                buf_remove(units, i--);
                did_react = 1;
            }
        }
    } while (did_react);
}

int main(int argc, char **argv)
{
    FILE *input_file = fopen("day5.input", "rb");
    if (!input_file)
    {
        fprintf(stderr, "Failed to open day5.input.\n");
        return 1;
    }

    char *units = 0;
    char buffer[BUFFER_SIZE] = {0};
    while (fgets(buffer, BUFFER_SIZE, input_file))
    {
        for (int i = 0;
             i < BUFFER_SIZE;
             ++i)
        {
            char c = buffer[i];
            if (c == 0) break;
            if (c != '\n')
            {
                buf_push(units, (char)c);
            }
        }
    }

    int smallest_unit_len = buf_len(units);
    for (int i = 'a';
         i <= 'z';
         ++i)
    {
        char *unit_copy = buf_copy(units);
        for (int j = 0;
             j < buf_len(unit_copy);
             ++j)
        {
            if (tolower(unit_copy[j]) == i)
            {
                buf_remove(unit_copy, j--);
            }
        }

        react(unit_copy);

        if (smallest_unit_len > buf_len(unit_copy))
        {
            smallest_unit_len = buf_len(unit_copy);
        }

        buf_free(unit_copy);
    }

    //react(units);
    //printf("Units: %d\n", buf_len(units));
    printf("Smallest Unit Len: %d\n", smallest_unit_len);

    return 0;
}
