//
//  day6.c
//  Advent of Code
//
//  Created by Helsper, Marshel J on 1/23/19.
//

#include <assert.h>
#include <stdio.h>

#include "common.h"

#define BUFFER_SIZE 4096

typedef struct
{
    int x, y;
} Point;

typedef struct
{
    Point point;
    int area;
    int hit_edge;
} CoordArea;

int manhattan_distance(Point a, Point b)
{
    int result = 0;
    int x_dist = a.x - b.x;
    int y_dist = a.y - b.y;

    if (x_dist < 0) x_dist = -x_dist;
    if (y_dist < 0) y_dist = -y_dist;

    result = x_dist + y_dist;

    return result;
}

int main(int argc, char **argv)
{
    FILE *input_file = fopen("day6.input", "rb");
    if (!input_file)
    {
        fprintf(stderr, "Failed to open day6.input.\n");
        return 1;
    }

    Point furthest_plane_coord = {0};
    CoordArea *coordinates = 0;
    char buffer[BUFFER_SIZE] = {0};
    while (fgets(buffer, BUFFER_SIZE, input_file))
    {
        CoordArea coord = {0};

        String coord_string = c_string_to_string(buffer);
        String *coord_parts = string_split(coord_string, ' ');

        coord.point.x = atoi(coord_parts[0].str);
        coord.point.y = atoi(coord_parts[1].str);

        if (coord.point.x > furthest_plane_coord.x) furthest_plane_coord.x = coord.point.x;
        if (coord.point.y > furthest_plane_coord.y) furthest_plane_coord.y = coord.point.y;

        buf_push(coordinates, coord);
    }

    int *distances = xmalloc(sizeof(int) * buf_len(coordinates));
    memset(distances, 0, buf_len(coordinates));
    printf("Distances Size: %d\n", buf_len(coordinates));

    Point *region = 0;

    for (int y = 0;
         y < furthest_plane_coord.y + 1;
         ++y)
    {
        for (int x = 0;
             x < furthest_plane_coord.x + 1;
             ++x)
        {
            Point current_point = {x, y};
            for (int i = 0;
                 i < buf_len(coordinates);
                 ++i)
            {
                Point coord_point = coordinates[i].point;
                distances[i] = manhattan_distance(current_point, coord_point);
            }

            int shortest_distance = distances[0];
            int shortest_distance_index = 0;
            int distance_sum = distances[0];
            for (int i = 1;
                 i < buf_len(coordinates);
                 ++i)
            {
                if (distances[i] < shortest_distance)
                {
                    shortest_distance = distances[i];
                    shortest_distance_index = i;
                }

                distance_sum += distances[i];
            }

            if (distance_sum < 10000)
            {
                buf_push(region, current_point);
            }

             for (int i = 0;
                  i < buf_len(coordinates);
                  ++i)
             {
                 if (i == shortest_distance_index) continue;
                 if (distances[i] == shortest_distance)
                 {
                     shortest_distance_index = -1;
                     break;
                 }
             }

            if (shortest_distance_index != -1)
            {
                ++coordinates[shortest_distance_index].area;

                if (y == 0 || y == furthest_plane_coord.y || x == 0 || x == furthest_plane_coord.x)
                {
                    coordinates[shortest_distance_index].hit_edge = 1;
                }
            }
        }
    }

    int largest_area = 0;
    for (int coord_index = 0;
         coord_index < buf_len(coordinates);
         ++coord_index)
    {
        CoordArea coord = coordinates[coord_index];
        if (!coord.hit_edge)
        {
            if (coord.area > largest_area)
            {
                largest_area = coord.area;
            }
        }
    }
    printf("Largest Area: %d\n", largest_area);
    printf("Safe Region Size: %d\n", buf_len(region));

    return 0;
}
