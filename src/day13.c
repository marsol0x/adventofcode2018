/* ==================================================================================
   $File: $
   $Date: 13 May 2019 $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

#include <assert.h>

#include "common.h"

enum
{
    Direction_Bottom,

    North,
    East,
    South,
    West,

    Direction_Top,
};

enum
{
    Left,
    Straight,
    Right,
};

typedef struct Cart
{
    i32 x;
    i32 y;
    i32 facing;
    i32 turn_option;
} Cart;
MERGE_SORT_DEFINE(Cart);

COMPARE_FUNC(compare_cart)
{
    Cart a_cart = *(Cart *)a;
    Cart b_cart = *(Cart *)b;

    if (a_cart.x == b_cart.x && a_cart.y == b_cart.y)
    {
        return 0;
    }

    if (a_cart.y < b_cart.y)
    {
        return -1;
    } else if (a_cart.y > b_cart.y) {
        return 1;
    }

    if (a_cart.x < b_cart.x)
    {
        return -1;
    } else if (a_cart.x > b_cart.x) {
        return 1;
    }

    assert(!"Comparison broke");
    return 0;
}

i32 did_collide(Cart *cart, Cart *carts)
{
    for (i32 cart_index = 0;
         cart_index < buf_len(carts);
         ++cart_index)
    {
        Cart *c = carts + cart_index;
        if (c == cart) continue;

        if (c->x == cart->x && c->y == cart->y)
        {
            return cart_index;
        }
    }

    return -1;
}
i32 main(i32 args, char **argv)
{
    FILE *input_file = fopen("day13.input", "rb");
    if (!input_file)
    {
        fprintf(stderr, "Failed to open day13.input.\n");
        return 1;
    }
    char *input_data = read_entire_file(input_file);
    i32 data_length = strlen(input_data);

    i32 num_columns = 0;
    i32 num_rows = 0;
    {
        i32 cols = 0;
        char *c = input_data;
        do
        {
            if (*c == '\0' || *c == '\r' || *c == '\n')
            {
                if (*c != '\0' && *c == '\r')
                {
                    ++c; // NOTE(marshel): Consume following \n
                    if (*c != '\n') 
                    {
                        fprintf(stderr, "Expected a following new line.\n");
                        return 2;
                    }
                }
                ++num_rows;
                if (cols > num_columns)
                {
                    num_columns = cols;
                }
                cols = 0;
                continue;
            }
            ++cols;
        } while (*c++);
    }

    Cart *carts = 0; // NOTE(marshel): Stretchy buf
    char *tracks = xmalloc((num_columns * num_rows + 1) * sizeof(char));
    memset(tracks, ' ', num_columns * num_rows);
    tracks[num_columns * num_rows] = 0;
#define GET_TRACK(x, y) tracks[(num_columns * y) + x]
    {
        i32 x = 0;
        i32 y = 0;
        char *c = input_data;
        do
        {
            if (*c == '\r' || *c == '\n') continue;

            switch (*c)
            {
                case '>':
                case '<':
                {
                    GET_TRACK(x, y) = '-';
                    Cart cart = {x, y, *c == '>' ? East : West, Left};
                    buf_push(carts, cart);
                } break;

                case '^':
                case 'v':
                {
                    GET_TRACK(x, y) = '|';
                    Cart cart = {x, y, *c == '^' ? North : South, Left};
                    buf_push(carts, cart);
                } break;

                default:
                {
                    GET_TRACK(x, y) = *c;
                } break;
            }
            ++x;

            if (x >= num_columns)
            {
                x = 0;
                ++y;
            }
        } while (*c++);
    }

    // NOTE(marshel): Tick
    {
        while (buf_len(carts) > 1)
        {
            MERGE_SORT(Cart, carts, compare_cart);
            for (i32 cart_index = 0;
                 cart_index < buf_len(carts);
                 ++cart_index)
            {
                Cart *cart = carts + cart_index;

                char current_track = GET_TRACK(cart->x, cart->y);
                switch (current_track)
                {
                    case '|':
                    {
                        if (cart->facing == North)
                        {
                            cart->y--;
                        } else {
                            cart->y++;
                        }
                    } break;

                    case '-':
                    {
                        if (cart->facing == East)
                        {
                            cart->x++;
                        } else {
                            cart->x--;
                        }
                    } break;

                    case '\\':
                    {
                        switch (cart->facing)
                        {
                            case North:
                            {
                                cart->facing = West;
                                cart->x--;
                            } break;

                            case South:
                            {
                                cart->facing = East;
                                cart->x++;
                            } break;

                            case East:
                            {
                                cart->facing = South;
                                cart->y++;
                            } break;

                            case West:
                            {
                                cart->facing = North;
                                cart->y--;
                            } break;
                        }
                    } break;

                    case '/':
                    {
                        switch (cart->facing)
                        {
                            case North:
                            {
                                cart->facing = East;
                                cart->x++;
                            } break;

                            case South:
                            {
                                cart->facing = West;
                                cart->x--;
                            } break;

                            case East:
                            {
                                cart->facing = North;
                                cart->y--;
                            } break;

                            case West:
                            {
                                cart->facing = South;
                                cart->y++;
                            } break;
                        }
                    } break;

                    case '+':
                    {
                        switch (cart->turn_option)
                        {
                            case Left:
                            {
                                cart->facing--;
                                cart->turn_option = Straight;
                            } break;

                            case Straight:
                            {
                                cart->turn_option = Right;
                            } break;

                            case Right:
                            {
                                cart->facing++;
                                cart->turn_option = Left;
                            } break;
                        }

                        if (cart->facing == Direction_Bottom)
                        {
                            cart->facing = West;
                        } else if (cart->facing == Direction_Top) {
                            cart->facing = North;
                        }

                        switch (cart->facing)
                        {
                            case North: { cart->y--; } break;
                            case South: { cart->y++; } break;
                            case East:  { cart->x++; } break;
                            case West:  { cart->x--; } break;
                        }
                    } break;

                    default:
                    {
                        assert(!"Invalid code path");
                    } break;
                }

                i32 collide_index = did_collide(cart, carts);
                if (collide_index != -1)
                {
                    printf("Collided: (%d, %d)\n", cart->x, cart->y);
                    if (collide_index > cart_index)
                    {
                        buf_remove(carts, collide_index);
                        buf_remove(carts, cart_index);
                        cart_index--;
                    } else {
                        buf_remove(carts, cart_index);
                        buf_remove(carts, collide_index);
                        cart_index -= 2;
                    }
                }
            }
        }
    }
    
    Cart last_cart = carts[0];
    printf("Last Cart: (%d, %d)\n", last_cart.x, last_cart.y);

    return 0;
}
